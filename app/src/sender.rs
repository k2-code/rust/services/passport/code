use rand::{thread_rng, Rng};

pub const MIN: u32 = 10000;
pub const MAX: u32 = 99999;

pub fn generate_code() -> u32 {
    thread_rng().gen_range(MIN..=MAX)
}

#[cfg(test)]
pub mod unit_tests {
    use super::*;

    #[test]
    fn generate_code_result_is_greater_than_or_equal_min() {
        assert!(generate_code() >= MIN);
    }

    #[test]
    fn generate_code_result_is_less_than_or_equal_max() {
        assert!(generate_code() <= MAX);
    }

    #[test]
    fn generate_codes_not_equal() {
        assert_ne!(generate_code(), generate_code());
    }
}
