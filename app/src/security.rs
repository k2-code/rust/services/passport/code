use argon2::Config;
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};

pub const UUID_LENGTH: u8 = 36;

pub fn encrypt(text: impl Into<String>) -> String {
    let salt: String = generate_string(None);
    let config: Config = Config::default();

    argon2::hash_encoded(text.into().as_bytes(), salt.as_bytes(), &config).unwrap()
}

pub fn verify(hash: &str, text: &str) -> bool {
    argon2::verify_encoded(hash, text.as_bytes()).unwrap()
}

pub fn generate_string(length: Option<usize>) -> String {
    let rand_string: String = thread_rng()
        .sample_iter(&Alphanumeric)
        .take(length.unwrap_or(15))
        .map(char::from)
        .collect();

    rand_string
}

#[cfg(test)]
mod unit_tests {
    use super::*;

    #[test]
    fn encrypt_works() {
        let str: &str = "test_str";

        assert_ne!(str, encrypt(str));
    }

    #[test]
    fn verify_works() {
        let str: &str = "test_str";
        let str_encrypted: String = encrypt(str);

        assert!(verify(&str_encrypted, str));
    }

    #[test]
    fn generate_string_have_string_with_length_15() {
        assert_eq!(generate_string(None).len(), 15);
    }

    #[test]
    fn generate_string_have_string_with_length_5() {
        assert_eq!(generate_string(Some(5)).len(), 5);
    }

    #[test]
    fn generate_strings_not_equal() {
        assert_ne!(generate_string(None), generate_string(None));
    }
}
