use crate::actions::{login, otp, register};
use actix_web::{web, web::Data, Scope};
use db::dynamo_db::passport::Passport;
use db::{Provider, DB};

pub type ServiceResult<T> = Result<T, Box<dyn std::error::Error>>;

pub fn get_configured_scope() -> Scope {
    web::scope(get_scope_path())
        .configure(login::action_config)
        .configure(otp::action_config)
        .configure(register::action_config)
}

pub fn get_scope_path<'a>() -> &'a str {
    "/passport"
}

pub fn get_db_passport_data() -> Data<DB<Passport>> {
    Data::new(DB::new(Passport::new()))
}
