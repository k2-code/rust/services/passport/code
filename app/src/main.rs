mod actions;
mod app;
mod jwt;
mod response;
mod security;
mod sender;

use actix_web::{App, HttpServer};

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let port: String = std::env::var("PORT").unwrap();

    HttpServer::new(move || {
        App::new()
            .app_data(app::get_db_passport_data())
            .service(app::get_configured_scope())
    })
    .bind(format!("0.0.0.0:{}", port))?
    .run()
    .await
}
