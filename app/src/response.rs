use actix_web::HttpResponse;
use derive_more::Display;
use serde::{Deserialize, Serialize};
use std::fmt::Display;

#[derive(Debug, Display)]
pub enum Message {
    #[display(fmt = "Confirmation code was sent")]
    ConfirmCodeSent,

    #[display(fmt = "Phone number was confirmed")]
    PhoneConfirmed,

    #[display(fmt = "Successfully registered")]
    Registered,

    #[display(fmt = "Already registered")]
    AlreadyRegistered,

    #[display(fmt = "Invalid credentials")]
    InvalidCredentials,

    #[display(fmt = "Too many login requests. Try again later")]
    MaxRequests,

    #[display(fmt = "Can't process your request. Please, write an email to support")]
    CannotProcessRequest,

    #[display(fmt = "Can't send SMS to your phone. Please, write an email to support")]
    CannotSendSms,

    #[display(fmt = "Invalid code or token")]
    InvalidCodeOrToken,

    #[display(
        fmt = "Slug should contain a-z, 0-9, underscores and have length between {} and {} characters. First name should have length between {} and {} characters",
        _0,
        _1,
        _2,
        _3
    )]
    InvalidSlugOrFirstName(u8, u8, u8, u8),

    #[display(fmt = "Confirmation code was expired. Try to login again")]
    ConfirmCodeExpired,
}

impl From<Message> for String {
    fn from(msg: Message) -> Self {
        msg.to_string()
    }
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Response {
    pub message: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub token: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub is_new: Option<bool>,
}

impl Response {
    pub fn new(msg: impl Into<String>, token: impl Into<String>) -> Self {
        Self {
            message: msg.into(),
            token: Some(token.into()),
            is_new: None,
        }
    }

    pub fn new_is_new(msg: impl Into<String>, token: impl Into<String>, is_new: bool) -> Self {
        Self {
            message: msg.into(),
            token: Some(token.into()),
            is_new: Some(is_new),
        }
    }
}

pub fn bad(msg: impl Display) -> HttpResponse {
    HttpResponse::BadRequest().json(Response {
        message: msg.to_string(),
        ..Default::default()
    })
}

pub fn forbidden(msg: impl Display) -> HttpResponse {
    HttpResponse::Forbidden().json(Response {
        message: msg.to_string(),
        ..Default::default()
    })
}

pub fn ok(body: impl Serialize) -> HttpResponse {
    HttpResponse::Ok().json(body)
}

#[cfg(test)]
mod unit_tests {
    use super::*;
    use actix_web::http::StatusCode;

    #[derive(Serialize, Default, Debug)]
    struct Credentials {
        value: String,
    }

    #[test]
    fn bad_have_bad_status() {
        assert_eq!(
            bad(Message::CannotProcessRequest).status(),
            StatusCode::BAD_REQUEST,
        );
    }

    #[test]
    fn forbidden_have_forbidden_status() {
        assert_eq!(
            forbidden(Message::CannotProcessRequest).status(),
            StatusCode::FORBIDDEN,
        );
    }

    #[test]
    fn ok_have_ok_status() {
        assert_eq!(ok(Credentials::default()).status(), StatusCode::OK);
    }
}
