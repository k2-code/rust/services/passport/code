use crate::jwt::generate_jwt;
use crate::response::{bad, ok, Message, Response};
use actix_web::web::{Data, Json};
use actix_web::{web, HttpResponse};
use auth::prelude::AuthData;
use db::dynamo_db::passport::Passport;
use db::models::user::{User, UserProvider, Where};
use db::{DBResult, DB};
use jwt::{JWTResult, UserClaims};
use lazy_static::lazy_static;
use regex::Regex;
use serde::{Deserialize, Serialize};
use validator::Validate;

pub const ROUTE: &str = "/register";

const SLUG_MIN_LENGTH: u8 = 4;
const SLUG_MAX_LENGTH: u8 = 32;

const FIRST_NAME_MIN_LENGTH: u8 = 1;
const FIRST_NAME_MAX_LENGTH: u8 = 64;

lazy_static! {
    static ref SLUG_REGEX: Regex = Regex::new(&format!(
        "[a-z0-9_]{{{}, {}}}$",
        SLUG_MIN_LENGTH, SLUG_MAX_LENGTH
    ))
    .unwrap();
}

#[derive(Serialize, Deserialize, Validate, Debug)]
pub struct Credentials {
    #[validate(
        required,
        length(min = "SLUG_MIN_LENGTH", max = "SLUG_MAX_LENGTH"),
        regex(path = "SLUG_REGEX")
    )]
    slug: Option<String>,
    #[validate(
        required,
        length(min = "FIRST_NAME_MIN_LENGTH", max = "FIRST_NAME_MAX_LENGTH")
    )]
    first_name: Option<String>,
}

pub fn action_config(cfg: &mut web::ServiceConfig) {
    cfg.service(web::resource(ROUTE).route(web::post().to(action)));
}

pub async fn action(
    credentials: Json<Credentials>,
    db: Data<DB<Passport>>,
    auth_data: AuthData,
) -> HttpResponse {
    if credentials.validate().is_err() {
        return bad(Message::InvalidSlugOrFirstName(
            SLUG_MIN_LENGTH,
            SLUG_MAX_LENGTH,
            FIRST_NAME_MIN_LENGTH,
            FIRST_NAME_MAX_LENGTH,
        ));
    }

    let Ok(mut user): DBResult<User> = get_user(&db, &auth_data).await else {
        eprintln!("Cannot load user '{}' for registration!", auth_data.user_id);
        return bad(Message::CannotProcessRequest);
    };

    if user.is_exists() {
        eprintln!("User {} is already registered", user.id().as_ref().unwrap());
        return bad(Message::AlreadyRegistered);
    }

    if get_user_by_slug(&db, &credentials).await.is_ok() {
        eprintln!(
            "Cannot register user '{}'! Selected slug already exists: '{}'!",
            auth_data.user_id,
            credentials.slug.as_ref().unwrap()
        );

        return bad(Message::AlreadyRegistered);
    }

    user.set_slug(credentials.slug.as_ref().unwrap().to_owned())
        .set_first_name(credentials.first_name.as_ref().unwrap().to_owned())
        .activate();

    if let Err(err) = db.upsert_user(&user).await {
        eprintln!("Cannot register user. Error: {:?}", err);
        return bad(Message::CannotProcessRequest);
    }

    let claims: UserClaims =
        UserClaims::id_slug(user.id().as_ref().unwrap(), user.slug().as_ref().unwrap());

    let Ok(jwt): JWTResult<String> = generate_jwt(claims) else {
        println!("Cannot generate JWT token!");
        return bad(Message::CannotProcessRequest);
    };

    ok(Response::new(Message::Registered, jwt))
}

async fn get_user(db: &Data<DB<Passport>>, auth_data: &AuthData) -> DBResult<User> {
    db.get_user(Where::Id(auth_data.user_id.to_owned())).await
}

async fn get_user_by_slug(db: &Data<DB<Passport>>, credentials: &Credentials) -> DBResult<User> {
    db.get_user(Where::Slug(credentials.slug.as_ref().unwrap().to_owned()))
        .await
}

#[cfg(test)]
mod unit_tests {
    use super::*;
    use crate::app;
    use actix_web::{http::StatusCode, test, HttpRequest, HttpResponse, Responder};
    use db::Provider;

    const TEST_USER_ID: &str = "test_user_id";

    fn get_correct_slug() -> String {
        (1..=SLUG_MIN_LENGTH).map(|_| "v".to_string()).collect()
    }

    fn get_correct_first_name() -> String {
        (1..=FIRST_NAME_MIN_LENGTH)
            .map(|_| "v".to_string())
            .collect()
    }

    async fn call_action(credentials: Credentials) -> HttpResponse {
        let request: HttpRequest = test::TestRequest::default().to_http_request();

        action(
            Json(credentials),
            app::get_db_passport_data(),
            AuthData {
                user_id: TEST_USER_ID.to_string(),
            },
        )
        .await
        .respond_to(&request)
    }

    #[actix_web::test]
    async fn action_have_bad_request_status_if_slug_length_is_less_than_min() {
        let slug: String = (1..SLUG_MIN_LENGTH).map(|_| "v".to_string()).collect();

        let response: HttpResponse = call_action(Credentials {
            slug: Some(slug),
            first_name: Some(get_correct_first_name()),
        })
        .await;

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }

    #[actix_web::test]
    async fn action_have_bad_request_status_if_slug_length_is_greater_than_max() {
        let slug: String = (0..=SLUG_MAX_LENGTH).map(|_| "v".to_string()).collect();

        let response: HttpResponse = call_action(Credentials {
            slug: Some(slug),
            first_name: Some(get_correct_first_name()),
        })
        .await;

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }

    #[actix_web::test]
    async fn action_have_bad_request_status_if_first_name_length_is_less_than_min() {
        let first_name: String = (1..FIRST_NAME_MIN_LENGTH)
            .map(|_| "v".to_string())
            .collect();

        let response: HttpResponse = call_action(Credentials {
            slug: Some(get_correct_slug()),
            first_name: Some(first_name),
        })
        .await;

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }

    #[actix_web::test]
    async fn action_have_bad_request_status_if_first_name_length_is_greater_than_max() {
        let first_name: String = (0..=FIRST_NAME_MAX_LENGTH)
            .map(|_| "v".to_string())
            .collect();

        let response: HttpResponse = call_action(Credentials {
            slug: Some(get_correct_slug()),
            first_name: Some(first_name),
        })
        .await;

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }

    #[actix_web::test]
    async fn action_have_bad_request_status_if_user_exists() -> DBResult<()> {
        let db: DB<Passport> = DB::new(Passport::new());

        let mut new_user: User = User::new_phone("+11111111111");

        new_user.set_id(TEST_USER_ID).activate();

        db.upsert_user(&new_user).await?;

        let response: HttpResponse = call_action(Credentials {
            slug: Some(get_correct_slug()),
            first_name: Some(get_correct_first_name()),
        })
        .await;

        db.delete_user(&new_user).await?;

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
        Ok(())
    }

    #[actix_web::test]
    async fn action_have_bad_request_status_if_user_slug_exists() -> DBResult<()> {
        let db: DB<Passport> = DB::new(Passport::new());
        let slug: String = get_correct_slug();

        let mut new_user: User = User::new_phone("+11111111111");

        new_user.set_id(TEST_USER_ID).set_slug(slug.clone());

        db.upsert_user(&new_user).await?;

        let response: HttpResponse = call_action(Credentials {
            slug: Some(slug),
            first_name: Some(get_correct_first_name()),
        })
        .await;

        db.delete_user(&new_user).await?;

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
        Ok(())
    }

    #[actix_web::test]
    async fn action_have_ok_status_if_slug_and_first_name_are_correct() -> DBResult<()> {
        let db: DB<Passport> = DB::new(Passport::new());

        let mut new_user: User = User::new_phone("+11111111111");

        new_user.set_id(TEST_USER_ID);

        db.upsert_user(&new_user).await?;

        let response: HttpResponse = call_action(Credentials {
            slug: Some(get_correct_slug()),
            first_name: Some(get_correct_first_name()),
        })
        .await;

        db.delete_user(&new_user).await?;

        assert_eq!(response.status(), StatusCode::OK);
        Ok(())
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use crate::app;
    use actix_web::{test, App};
    use db::Provider;
    use jwt::{JWTResult, UserClaims};

    const TEST_USER_ID: &str = "test_user_id";

    fn get_correct_slug() -> String {
        (1..=SLUG_MIN_LENGTH).map(|_| "v".to_string()).collect()
    }

    fn get_correct_first_name() -> String {
        (1..=FIRST_NAME_MIN_LENGTH)
            .map(|_| "v".to_string())
            .collect()
    }

    #[actix_web::test]
    async fn action_have_response_data_if_correct_slug_first_name_and_jwt() -> DBResult<()> {
        let app = test::init_service(
            App::new()
                .app_data(app::get_db_passport_data())
                .service(app::get_configured_scope()),
        )
        .await;

        let db: DB<Passport> = DB::new(Passport::new());

        let mut new_user: User = User::new_phone("+11111111111");

        new_user.set_id(TEST_USER_ID);

        let token: JWTResult<String> =
            jwt::generate(UserClaims::id(new_user.id().as_ref().unwrap()), None);

        assert!(token.is_ok());

        db.upsert_user(&new_user).await?;

        let slug: String = get_correct_slug();
        let first_name: String = get_correct_first_name();

        let request = test::TestRequest::post()
            .uri(&format!("{}{}", app::get_scope_path(), ROUTE))
            .insert_header(("authorization", format!("Bearer {}", token.unwrap())))
            .set_json(Credentials {
                slug: Some(slug),
                first_name: Some(first_name),
            })
            .to_request();

        let response: Response = test::call_and_read_body_json(&app, request).await;

        db.delete_user(&new_user).await?;

        assert!(response.message.contains(&Message::Registered.to_string()));
        Ok(())
    }
}
