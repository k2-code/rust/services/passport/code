use crate::response::{bad, ok, Message, Response};
use crate::{security, sender};
use actix_web::web::{Data, Json};
use actix_web::{web, HttpResponse};
use db::dynamo_db::passport::Passport;
use db::models::login_request::{LoginRequest, LoginRequestProvider, Where};
use db::{DBResult, DB};
use queue::event::Event;
use queue::{ymq::YMQ, Queue, Sender};
use serde::{Deserialize, Serialize};
use std::env;
use validator::{Validate, ValidationError};

pub const ROUTE: &str = "/login";

#[derive(Serialize, Deserialize, Validate, Debug)]
pub struct Credentials {
    #[validate(required, custom = "validate_email_or_phone")]
    value: Option<String>,
}

pub fn action_config(cfg: &mut web::ServiceConfig) {
    cfg.service(web::resource(ROUTE).route(web::post().to(action)));
}

pub async fn action(credentials: Json<Credentials>, db: Data<DB<Passport>>) -> HttpResponse {
    if credentials.validate().is_err() {
        return bad(Message::InvalidCredentials);
    }

    let value: &str = credentials.value.as_ref().unwrap();
    let result: DBResult<LoginRequest> = db.get_login_request(Where::Value(value.to_owned())).await;

    if let Ok(ref login_request) = result {
        if login_request.is_attempts_full() {
            return bad(Message::MaxRequests);
        }
    }

    let code: String = sender::generate_code().to_string();

    let login_request: LoginRequest =
        prepare_login_request(result, value, &security::encrypt(&code));

    if db.upsert_login_request(&login_request).await.is_err() {
        println!("Cannot upsert login request: {:?}!", login_request);
        return bad(Message::CannotProcessRequest);
    };

    let event: String = Event::passport_login(value, get_event_msg(&code))
        .to_json()
        .unwrap();

    // TODO: remove
    println!("Sending {event} to queue service...");

    if get_queue().send(&event).await.is_err() {
        println!("Cannot send {event} to queue service!");
        return bad(Message::CannotSendSms);
    }

    ok(Response::new(
        Message::ConfirmCodeSent,
        login_request.id().as_ref().unwrap(),
    ))
}

fn get_queue() -> Queue<YMQ> {
    Queue::new(YMQ::new(
        env::var("QUEUE_ENDPOINT_URL").expect("No endpoint url defined!"),
        env::var("QUEUE_PASSPORT_LOGIN_PATH").expect("No passport login path defined!"),
        env::var("QUEUE_REGION_TITLE").expect("No region title defined!"),
        env::var("QUEUE_PASSPORT_LOGIN_ACCESS_KEY").expect("No passport login access key defined!"),
        env::var("QUEUE_PASSPORT_LOGIN_SECRET_KEY").expect("No passport login secret key defined!"),
    ))
}

fn get_event_msg(code: &str) -> String {
    format!(
        // TODO: translate
        "This is your confirmation code: {code}. Don't tell it anyone!"
    )
}

fn validate_email_or_phone(value: &str) -> Result<(), ValidationError> {
    if validator::validate_email(value) || validator::validate_phone(value) {
        return Ok(());
    }

    Err(ValidationError::new("Email or phone is invalid!"))
}

fn prepare_login_request(
    result: DBResult<LoginRequest>,
    value: impl Into<String>,
    encrypted_code: impl Into<String>,
) -> LoginRequest {
    match result {
        Ok(mut login_request) => {
            login_request.add_attempt();
            login_request
        }

        Err(_) => LoginRequest::new_value_code(value, encrypted_code),
    }
}

#[cfg(test)]
mod unit_tests {
    use super::*;
    use crate::app;
    use actix_web::{http::StatusCode, test, HttpRequest, HttpResponse, Responder};
    use db::Provider;

    const PHONE: &str = "+14152370800";

    async fn clean_lr(value: impl Into<String>) -> DBResult<()> {
        let db: DB<Passport> = DB::new(Passport::new());
        let lr: LoginRequest = db.get_login_request(Where::Value(value.into())).await?;

        db.delete_login_request(&lr).await
    }

    async fn call_action(credentials: Credentials) -> HttpResponse {
        let request: HttpRequest = test::TestRequest::default().to_http_request();

        action(Json(credentials), app::get_db_passport_data())
            .await
            .respond_to(&request)
    }

    #[actix_web::test]
    async fn action_have_bad_request_status_if_no_value() {
        let response: HttpResponse = call_action(Credentials {
            value: Some("".to_string()),
        })
        .await;

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }

    #[actix_web::test]
    async fn action_have_bad_request_status_if_wrong_value() {
        let response: HttpResponse = call_action(Credentials {
            value: Some("+19999999999".to_string()),
        })
        .await;

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }

    #[actix_web::test]
    async fn action_have_ok_status_if_correct_value() -> DBResult<()> {
        let response: HttpResponse = call_action(Credentials {
            value: Some(PHONE.to_string()),
        })
        .await;

        clean_lr(PHONE).await?;

        assert_eq!(response.status(), StatusCode::OK);

        Ok(())
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use crate::app;
    use actix_web::{test, App};
    use db::Provider;

    #[actix_web::test]
    async fn action_have_response_data_if_correct_value() -> DBResult<()> {
        let value: &str = "+14152370801";

        let app = test::init_service(
            App::new()
                .app_data(app::get_db_passport_data())
                .service(app::get_configured_scope()),
        )
        .await;

        let request = test::TestRequest::post()
            .uri(&format!("{}{}", app::get_scope_path(), ROUTE))
            .set_json(Credentials {
                value: Some(value.to_string()),
            })
            .to_request();

        let response: Response = test::call_and_read_body_json(&app, request).await;

        assert!(response
            .message
            .contains(&Message::ConfirmCodeSent.to_string()));

        assert!(response.token.is_some());
        assert_eq!(response.token.unwrap().len() as u8, security::UUID_LENGTH);

        let db: DB<Passport> = DB::new(Passport::new());

        let lr: LoginRequest = db
            .get_login_request(Where::Value(value.to_string()))
            .await?;

        db.delete_login_request(&lr).await?;

        Ok(())
    }
}
