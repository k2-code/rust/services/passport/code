use crate::app::ServiceResult;
use crate::jwt::generate_jwt;
use crate::response::{bad, forbidden, ok, Message, Response};
use crate::{security, sender};
use actix_web::web::{Data, Json};
use actix_web::{web, HttpResponse};
use db::dynamo_db::passport::Passport;
use db::models::login_request::{AuthType, LoginRequest, LoginRequestProvider, Where as LRWhere};
use db::models::user::{User, UserProvider, Where as UWhere};
use db::{DBResult, DB};
use jwt::{JWTResult, UserClaims};
use serde::{Deserialize, Serialize};
use validator::Validate;

pub const ROUTE: &str = "/login/otp";

#[derive(Serialize, Deserialize, Validate, Debug)]
pub struct Credentials {
    #[validate(required, range(min = "sender::MIN", max = "sender::MAX"))]
    code: Option<u32>,
    #[validate(required, length(equal = "security::UUID_LENGTH"))]
    token: Option<String>,
}

pub fn action_config(cfg: &mut web::ServiceConfig) {
    cfg.service(web::resource(ROUTE).route(web::post().to(action)));
}

pub async fn action(credentials: Json<Credentials>, db: Data<DB<Passport>>) -> HttpResponse {
    if credentials.validate().is_err() {
        return bad(Message::InvalidCodeOrToken);
    }

    let Ok(mut login_request): DBResult<LoginRequest> = get_login_request(&db, &credentials).await else {
        return bad(Message::ConfirmCodeExpired);
    };

    if login_request.is_confirm_attempts_full() {
        return forbidden(Message::MaxRequests);
    }

    if !process_verify(&db, &credentials, &mut login_request).await {
        return bad(Message::InvalidCodeOrToken);
    }

    if db.delete_login_request(&login_request).await.is_err() {
        println!("Cannot delete login request!");
        return bad(Message::ConfirmCodeExpired);
    }

    let user: User = prepare_user(&db, &login_request).await;

    let Ok(is_new) = process_new_user(&db, &user).await else {
        println!("Cannot process new user!");
        return bad(Message::CannotProcessRequest);
    };

    let Ok(jwt): JWTResult<String> = generate_jwt(UserClaims::id(user.id().as_ref().unwrap())) else {
        println!("Cannot generate JWT token!");
        return bad(Message::CannotProcessRequest);
    };

    ok(Response::new_is_new(Message::PhoneConfirmed, jwt, is_new))
}

async fn process_new_user(db: &Data<DB<Passport>>, user: &User) -> ServiceResult<bool> {
    if user.is_exists() {
        return Ok(false);
    }

    db.upsert_user(user).await?;
    Ok(true)
}

async fn process_verify(
    db: &Data<DB<Passport>>,
    credentials: &Json<Credentials>,
    login_request: &mut LoginRequest,
) -> bool {
    if !security::verify(
        login_request.code().as_ref().unwrap(),
        &credentials.code.unwrap().to_string(),
    ) {
        login_request.add_confirm_attempt();

        if db.upsert_login_request(login_request).await.is_err() {
            println!("Cannot update login request confirm attempt!");
        }

        return false;
    }

    true
}

async fn get_login_request(
    db: &Data<DB<Passport>>,
    credentials: &Credentials,
) -> DBResult<LoginRequest> {
    db.get_login_request(LRWhere::Id(credentials.token.as_ref().unwrap().to_owned()))
        .await
}

async fn prepare_user(db: &Data<DB<Passport>>, login_request: &LoginRequest) -> User {
    let value: &str = login_request.value().as_ref().unwrap();

    match login_request.get_auth_type() {
        AuthType::Email => {
            let Ok(user) = db.get_user(UWhere::Email(value.to_owned())).await else {
                return User::new_email(value);
            };

            user
        }

        AuthType::Phone => {
            let Ok(user) = db.get_user(UWhere::Phone(value.to_owned())).await else {
                return User::new_phone(value);
            };

            user
        }
    }
}

#[cfg(test)]
mod unit_tests {
    use super::*;
    use crate::app;
    use actix_web::{http::StatusCode, test, HttpRequest, HttpResponse, Responder};
    use db::models::user::Where;
    use db::Provider;

    const UUID: &str = "9e64f9c7-c14a-4661-8bab-e493105d1b23";

    async fn call_action(credentials: Credentials) -> HttpResponse {
        let request: HttpRequest = test::TestRequest::default().to_http_request();

        action(Json(credentials), app::get_db_passport_data())
            .await
            .respond_to(&request)
    }

    #[actix_web::test]
    async fn action_have_bad_request_status_if_code_is_less_than_min() {
        let response: HttpResponse = call_action(Credentials {
            code: Some(sender::MIN - 1),
            token: Some(UUID.to_string()),
        })
        .await;

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }

    #[actix_web::test]
    async fn action_have_bad_request_status_if_code_is_greater_than_max() {
        let response: HttpResponse = call_action(Credentials {
            code: Some(sender::MAX + 1),
            token: Some(UUID.to_string()),
        })
        .await;

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }

    #[actix_web::test]
    async fn action_have_bad_request_status_if_uuid_have_wrong_length() {
        let response: HttpResponse = call_action(Credentials {
            code: Some(12982),
            token: Some("wrong-uuid-length".to_string()),
        })
        .await;

        assert_eq!(response.status(), StatusCode::BAD_REQUEST);
    }

    #[actix_web::test]
    async fn action_have_ok_status_if_code_and_uuid_are_correct() -> DBResult<()> {
        let code: u32 = 12982;
        let value: &str = "+14152370802";

        let db: DB<Passport> = DB::new(Passport::new());
        let lr: LoginRequest =
            LoginRequest::new_value_code(value, &security::encrypt(code.to_string()));

        db.upsert_login_request(&lr).await?;

        let response: HttpResponse = call_action(Credentials {
            code: Some(code),
            token: lr.id().to_owned(),
        })
        .await;

        let user: User = db.get_user(Where::Phone(value.to_string())).await?;

        db.delete_user(&user).await?;

        assert_eq!(response.status(), StatusCode::OK);

        Ok(())
    }
}

#[cfg(test)]
mod integration_tests {
    use super::*;
    use crate::app;
    use actix_web::{test, App};
    use db::models::user::Where;
    use db::Provider;

    #[actix_web::test]
    async fn action_have_response_data_if_correct_code_and_uuid() -> DBResult<()> {
        let app = test::init_service(
            App::new()
                .app_data(app::get_db_passport_data())
                .service(app::get_configured_scope()),
        )
        .await;

        let code: u32 = 12982;
        let phone: &str = "+14152370803";

        let db: DB<Passport> = DB::new(Passport::new());
        let lr: LoginRequest =
            LoginRequest::new_value_code(phone, &security::encrypt(code.to_string()));

        db.upsert_login_request(&lr).await?;

        let request = test::TestRequest::post()
            .uri(&format!("{}{}", app::get_scope_path(), ROUTE))
            .set_json(Credentials {
                code: Some(code),
                token: lr.id().to_owned(),
            })
            .to_request();

        let response: Response = test::call_and_read_body_json(&app, request).await;
        let user: User = db.get_user(Where::Phone(phone.to_string())).await?;

        db.delete_user(&user).await?;

        assert!(response
            .message
            .contains(&Message::PhoneConfirmed.to_string()));

        assert!(response.token.is_some());

        Ok(())
    }
}
