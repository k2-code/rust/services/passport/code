use crate::app::ServiceResult;
use jwt::{Duration, UserClaims};

pub fn generate_jwt(claims: UserClaims) -> ServiceResult<String> {
    jwt::generate(claims, Some(Duration::from_days(365)))
}
